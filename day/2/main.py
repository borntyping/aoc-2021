import pathlib

path = pathlib.Path(__file__).with_name('input')
pairs = [line.split() for line in path.read_text().splitlines()]
steps = [(direction, int(distance)) for direction, distance in pairs]

horizontal = 0
depth = 0
aim = 0

for direction, distance in steps:
    if direction == 'down':
        aim += distance
    elif direction == 'up':
        aim -= distance
    elif direction == 'forward':
        horizontal += distance
        depth += aim * distance

print(f"{horizontal=} {depth=}")
print(horizontal * depth)
