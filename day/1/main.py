import pathlib

path = pathlib.Path(__file__).with_name('input')
numbers = [int(line) for line in path.read_text().splitlines()]

count = 0

for i in range(len(numbers)):
    previous_window = numbers[i+0:i+3]
    next_window = numbers[i+1:i+4]

    if path.name == 'example' and i == 0:
        assert previous_window == [199, 200, 208], previous_window
        assert next_window == [200, 208, 210], next_window

    if len(next_window) < 3:
        break

    if sum(next_window) > sum(previous_window):
        count += 1

print(count)
