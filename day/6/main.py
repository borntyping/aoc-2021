import collections

import progressbar

import aoc


def one(text: str) -> int:
    shoal = [int(fish) for fish in text.strip().split(",")]

    for _ in progressbar.progressbar(range(1, 80 + 1)):
        refresh = []
        for fish in shoal:
            if (fish - 1) == -1:
                refresh.append(6)
                refresh.append(8)
            else:
                refresh.append(fish - 1)
        shoal = refresh

    return len(shoal)


def two(text: str) -> int:
    counter = collections.Counter(int(fish) for fish in text.strip().split(","))
    shoal = [counter[n] for n in range(9)]

    for _ in progressbar.progressbar(range(1, 256 + 1)):
        spawn = shoal[0]
        shoal = [*shoal[1:], 0]
        shoal[6] += spawn
        shoal[8] += spawn

    return sum(shoal)


if __name__ == "__main__":
    assert aoc.example(one) == 5934
    assert aoc.example(two) == 26984457539

    aoc.puzzles(one, two)
