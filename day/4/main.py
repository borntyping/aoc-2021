import typing

import aoc
import dataclasses


@dataclasses.dataclass(frozen=True)
class Grid:
    id: int
    numbers: dict

    def __str__(self):
        string = ""
        for x in range(5):
            for y in range(5):
                n = self.numbers[(x, y)]
                string += f"{n:>2} "
            if x != 4:
                string += "\n"
        return string

    def rows(self) -> list[set[int]]:
        return [{self.numbers[(x, y)] for y in range(5)} for x in range(5)]

    def columns(self) -> list[set[int]]:
        return [{self.numbers[(x, y)] for x in range(5)} for y in range(5)]

    def wins(self, sequence: typing.Sequence[int]) -> bool:
        return any(winner.issubset(set(sequence)) for winner in [*self.rows(), *self.columns()])

    def score(self, sequence: typing.Sequence[int]) -> int:
        unmarked = set(self.numbers.values()) - set(sequence)
        return sum(unmarked) * sequence[-1]


@dataclasses.dataclass(frozen=True)
class Bingo:
    sequence: typing.Sequence[int]
    grids: typing.Sequence[Grid]

    def draws(self):
        for n in range(1, len(self.sequence) + 1):
            yield self.sequence[:n]


def parse(text: str) -> Bingo:
    sections = text.split("\n\n")
    sequence = [int(n) for n in sections[0].split(",")]

    grids = [
        Grid(
            id=id,
            numbers={
                (x, y): int(n)
                for x, line in enumerate(section.splitlines())
                for y, n in enumerate(line.split())
            },
        )
        for id, section in enumerate(sections[1:])
    ]

    return Bingo(sequence, grids)


def test(text: str) -> None:
    assert {14, 21, 17, 24, 4} in parse(text).grids[2].rows()


def one(text: str) -> int:
    bingo = parse(text)

    for sequence in bingo.draws():
        for grid in bingo.grids:
            if grid.wins(sequence):
                return grid.score(sequence)

    raise Exception("Did not find a winning grid")


def two(text: str) -> int:
    bingo = parse(text)
    winners = []
    scores = {}

    for sequence in bingo.draws():
        for grid in bingo.grids:
            if grid.wins(sequence):
                if grid not in winners:
                    winners.append(grid)
                    scores[grid.id] = grid.score(sequence)

    return scores[winners[-1].id]


if __name__ == "__main__":
    aoc.test(test)

    assert aoc.example(one) == 4512
    aoc.puzzles(one)

    assert aoc.example(two) == 1924
    aoc.puzzles(two)
