from __future__ import annotations

import dataclasses
import typing

import aoc

# {segments: number}
unique_lengths: typing.Mapping[int, int] = {
    2: 1,
    3: 7,
    4: 4,
    7: 8,
}


@dataclasses.dataclass()
class Display:
    unique_signal_patterns: typing.Tuple[str, str, str, str, str, str, str, str, str, str]
    output_value: typing.Tuple[str, str, str, str]

    def __post_init__(self) -> None:
        assert len(self.unique_signal_patterns) == 10, self.unique_signal_patterns
        assert len(self.output_value) == 4, self.output_value

    @classmethod
    def parse(cls, string: str) -> Display:
        unique_signal_patterns, output_value = string.split("|")
        return cls(
            unique_signal_patterns=unique_signal_patterns.strip().split(),
            output_value=output_value.strip().split(),
        )


def one(text: str) -> int:
    displays = [Display.parse(line) for line in text.splitlines()]
    raise NotImplementedError


if __name__ == "__main__":
    assert aoc.example(one) == 26
    assert aoc.example(two) == 61229
    aoc.puzzle(one)
    aoc.puzzle(two)
