import collections

import aoc


def one(lines: list[str]) -> int:
    counters = [collections.Counter(column) for column in zip(*lines)]
    gamma_rate_binary: str = ""
    epsilon_rate_binary: str = ""
    for counter in counters:
        most_common = counter.most_common()
        gamma_rate_binary += most_common[0][0]
        epsilon_rate_binary += most_common[1][0]
    gamma_rate = int(gamma_rate_binary, 2)
    epsilon_rate = int(epsilon_rate_binary, 2)
    power_consumption = gamma_rate * epsilon_rate
    return power_consumption


def find_bit_criteria(lines: list[str], *, index: int, rating: str) -> tuple[str, str]:
    counter = collections.Counter(line[index] for line in lines)
    equally_common = counter['0'] == counter['1']
    most_common, least_common = [x for x, _ in counter.most_common()]

    if rating == 'oxygen_generator_rating':
        return most_common if not equally_common else '1'
    elif rating == 'co2_scrubber_rating':
        return least_common if not equally_common else '0'

    raise NotImplementedError


def search(lines: list[str], *, rating: str) -> int:
    for index in range(13):
        bit_criteria = find_bit_criteria(lines, index=index, rating=rating)

        lines = [line for line in lines if line[index] == bit_criteria]

        if len(lines) == 1:
            return int(lines[0], 2)

        if len(lines) == 0:
            raise Exception('Empty search set')

        print(f"{index=} {bit_criteria=} {len(lines)=}")


def two(lines: list[str]) -> int:
    oxygen_generator_rating = search(lines, rating='oxygen_generator_rating')
    co2_scrubber_rating = search(lines, rating='co2_scrubber_rating')
    life_support_rating = oxygen_generator_rating * co2_scrubber_rating
    return life_support_rating


if __name__ == '__main__':
    aoc.puzzles(one, two)
