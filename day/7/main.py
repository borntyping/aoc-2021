import progressbar
import pytest

import aoc


def one(text: str) -> int:
    positions = [int(n) for n in text.strip().split(",")]
    return min(sum([abs(p - t) for p in positions]) for t in range(max(positions)))


def cost(a, b) -> int:
    return sum(range(1, abs(a - b) + 1))


def two(text: str) -> int:
    positions = [int(n) for n in text.strip().split(",")]
    targets = progressbar.progressbar(range(max(positions)))
    return min(sum([cost(p, t) for p in positions]) for t in targets)


@pytest.mark.parametrize(
    ("a", "b", "t"),
    [
        (16, 5, 66),
        (1, 5, 10),
        (2, 5, 6),
        (0, 5, 15),
        (4, 5, 1),
        (2, 5, 6),
        (7, 5, 3),
        (1, 5, 10),
        (2, 5, 6),
        (14, 5, 45),
    ],
)
def test_cost(a: int, b: int, t: int) -> None:
    assert cost(a, b) == t


if __name__ == "__main__":
    assert aoc.example(one) == 37
    assert aoc.example(two) == 168
    aoc.puzzles(one)
    aoc.puzzles(two)
