import aoc


def one(text: str) -> int:
    return 0


def two(text: str) -> int:
    return 0


if __name__ == "__main__":
    # assert aoc.example(one) == 0
    # assert aoc.example(two) == 0
    aoc.puzzles(one, two)
