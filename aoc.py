import pathlib
import inspect
import typing


Solution = typing.Callable[[str], int]
Test = typing.Callable[[str], None]


def load(function: typing.Callable, filename: str) -> typing.Tuple[str, str]:
    path = pathlib.Path(inspect.getfile(function))
    name = f"{path.relative_to(pathlib.Path.cwd())}::{function.__name__} input={filename}"
    return name, path.with_name(filename).read_text()


def run(function: Solution, filename: str) -> int:
    name, text = load(function, filename)
    print(f"\033[94m--- {name} \033[0m\n")
    result = function(text)
    print(f"{result=}\n")
    return result


def test(function: Test, filename: str = "example") -> None:
    _, text = load(function, filename=filename)
    function(text)


def example(function: Solution) -> int:
    return run(function, filename="example")


def puzzle(function: Solution) -> int:
    return run(function, filename="input")


def puzzles(*functions: Solution) -> None:
    for function in functions:
        puzzle(function=function)
